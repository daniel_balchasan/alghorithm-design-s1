import networkx as nx
import os
import matplotlib.pyplot as plt


def load_graphs(dir="data"):
    Gs = []
    file_names = os.listdir(dir)
    file_names.remove('.DS_Store')
    for file_name in file_names:
        if "ski" in file_name or "increase" in file_name:
            G = nx.DiGraph()
        else:
            G = nx.Graph()
        G.graph['name'] = file_name
        lines = [line.strip() for line in open(dir + "/" + file_name)]
        nmr = lines.pop(0).split()
        num_vertices = int(nmr[0])
        num_edges = int(nmr[1])
        num_reds = int(nmr[2])
        st = lines.pop(0).split()
        source = st[0]
        sink = st[1]
        G.add_node(source, is_source=True)
        G.graph['source'] = source
        G.graph['sink'] = sink
        G.add_node(sink, is_sink=True)
        for _ in range(num_vertices):
            vertex_properties = lines.pop(0).split()
            if len(vertex_properties) == 1:
                vcolor = "black"
            else:
                vcolor = "red"
            G.add_node(vertex_properties[0], color=vcolor)
        for _ in range(num_edges):
            udv = lines.pop(0).split()
            G.add_edge(udv[0], udv[2])
        Gs.append(G)
    return Gs


def draw_graph(G):
    print(G.graph['name'])
    color_map = []
    lables_dict = {}
    for node,data in G.nodes(data=True):
        color_map.append(data['color'])
        if G.graph["source"] == node:
            lables_dict[node] = 'S'
        elif G.graph["sink"] == node:
            lables_dict[node] = 'T'
        else:
            lables_dict[node] = node

    nx.draw(G, with_labels=True, labels=lables_dict, font_weight='bold', node_color=color_map, font_color='white', )
    plt.show()


def none(G):
    filtered_nodes = []
    for node, properties in G.nodes(data=True):
        if properties['color'] != 'red' or G.graph['source'] == node or G.graph['sink'] == node:
            filtered_nodes.append(node)
    subview = G.subgraph(filtered_nodes)
    try:
        path_length = nx.shortest_path_length(subview, G.graph['source'], G.graph['sink'])
    except nx.exception.NetworkXNoPath:
        path_length = -1
    return path_length


def some(G):
    if type(G) == nx.DiGraph:
        return '-'
    aux_con = nx.algorithms.connectivity.utils.build_auxiliary_node_connectivity(G)
    aux_con.graph['name'] = G.graph['name'] + " aux_node_con"
    aux_con.graph['source'] = str(aux_con.graph['mapping'][G.graph['source']]) + 'B'
    aux_con.graph['sink'] = str(aux_con.graph['mapping'][G.graph['sink']]) + 'B'

    for node, properties in G.nodes(data=True):
        name_in_new_graph = str(aux_con.graph['mapping'][node])
        aux_con.nodes[name_in_new_graph + 'A']['color'] = 'black'
        if properties['color'] == 'red':
            aux_con.nodes[name_in_new_graph+'B']['color'] = 'red'
        else:
            aux_con.nodes[name_in_new_graph + 'B']['color'] = 'black'

    aux_con.add_node("sinktag", color='black')
    aux_con.add_edge(aux_con.graph['source'], "sinktag", capacity=1)
    aux_con.add_edge(aux_con.graph['sink'], "sinktag", capacity=1)
    for node, properties in aux_con.nodes(data=True):
        if aux_con.nodes[node]['color'] == 'red':
            max_flow = nx.maximum_flow_value(aux_con, node, "sinktag")
            if max_flow == 2:
                return True
    return False


def few(G):
    directed_g = G.to_directed()
    for edge in directed_g.edges():
        if directed_g.nodes[edge[0]]['color'] == 'red':
            directed_g[edge[0]][edge[1]]['weight'] = 1
    try:
        path = nx.dijkstra_path(directed_g, directed_g.graph['source'], directed_g.graph['sink'])
        red_num = 0
        for node in path:
            if directed_g.node[node]['color'] == 'red':
                red_num += 1
    except nx.exception.NetworkXNoPath:
        red_num = -1
    return red_num


def many(G):
    '''
    Many is at least NP complete: Proof by reduction -
    Let G be a graph where all vertices are red where n = number of vertices.
    Determining if max == n is equivalent to determining if there exists an Hamiltonian Path between S-T.
    Implementing max in polynomial time would mean we can find Hamiltonian paths in polynomial time.
    Therefore, Hamiltonian Paths can be reduced to max.
    This means max is NP-Hard.
    '''
    return '-'


def alternate(G):
    filtered_edges = []
    for edge in G.edges():
        if G.nodes[edge[0]]['color'] != G.nodes[edge[1]]['color']:
            filtered_edges.append(edge)
    subview = G.edge_subgraph(filtered_edges)
    try:
        has_path = nx.has_path(subview, G.graph['source'], G.graph['sink'])
    except nx.exception.NodeNotFound:
        has_path = False
    return has_path


m = 0
Gs = load_graphs("data")
print(len(Gs))
with open('results.txt', 'w+') as f:
    for g in Gs:
        print(g.graph['name'])
        print(m)
        m += 1
        f.write('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\n'.format(
            g.graph['name'][:-4],
            g.number_of_nodes(),
            alternate(g),
            few(g),
            many(g),
            none(g),
            some(g)
        ))
