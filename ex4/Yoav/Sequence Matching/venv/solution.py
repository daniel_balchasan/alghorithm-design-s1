import itertools

def load_matrix(matrix_filename):
    with open(matrix_filename) as matrix_file:
      matrix = matrix_file.read()
    lines = matrix.strip().split('\n')

    header = lines.pop(0)
    columns = header.split()
    matrix = {}

    for row in lines:
      entries = row.split()
      row_name = entries.pop(0)
      matrix[row_name] = {}

      if len(entries) != len(columns):
        raise Exception('Improper entry number in row')
      for column_name in columns:
        matrix[row_name][column_name] = entries.pop(0)

    return matrix

def load_animals(animals_filename):
    with open(animals_filename) as animals_file:
      animals = animals_file.read()
    lines = animals.strip().split('\n')
    animalsArray = []
    stringAcc = ""
    for row in lines:
        entries = row.split()
        if row[0][0] == ">":
            # next animal, put string and continue
            if stringAcc != "":
                animalsArray[-1].append(stringAcc)
                stringAcc = ""
            animalsArray.append([entries[0][1:]])
        else:
            stringAcc += row

    if stringAcc != "":
        animalsArray[-1].append(stringAcc)
    return animalsArray

def pretty_print(mat):
    s = [[str(e) for e in row] for row in mat]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    print('\n'.join(table))

matrix = load_matrix("blosum.txt")
space_value = int(matrix['A']['*'])

def init_solution_matrix(string1, string2):
    solution_matrix = []
    for i in range(len(string1) + 1):
        row = []
        for j in range(len(string2) + 1):
            if i == 0:
                row.append(space_value * j)
            elif j == 0:
                row.append(space_value * i)
            else:
                row.append(None)
        solution_matrix.append(row)
    return solution_matrix

def solve(string1, string2):

    solution_matrix = init_solution_matrix(string1, string2)
    def opt(string1_index,string2_index):
         if solution_matrix[string1_index][string2_index] == None:
            string1space = space_value + opt(string1_index-1, string2_index)
            string2space = space_value + opt(string1_index, string2_index-1)
            replace = int(matrix[string1[string1_index-1]][string2[string2_index-1]]) + opt(string1_index-1, string2_index-1)
            return max(max(string1space, string2space), replace)
         else:
            return solution_matrix[string1_index][string2_index]

    for i in range(0, len(string1)+1):
        for j in range(0, len(string2)+1):
            solution_matrix[i][j] = opt(i,j)

    return solution_matrix

def print_solution(first_animal_name,string1, second_animal_name,string2, solution_matrix):
    alignment_a = ""
    alignment_b = ""
    i = len(string1)
    j = len(string2)
    while i>0 or j>0:
        if i > 0 and j > 0 and solution_matrix[i][j] == solution_matrix[i-1][j-1] + int(matrix[string1[i-1]][string2[j-1]]):
            alignment_a = string1[i-1] + alignment_a
            alignment_b = string2[j-1] + alignment_b
            i -= 1
            j -= 1
        elif i > 0 and solution_matrix[i][j] == solution_matrix[i-1][j] + space_value:
            alignment_a = string1[i-1] + alignment_a
            alignment_b = "-" + alignment_b
            i -= 1
        else:
            alignment_a = "-" + alignment_a
            alignment_b = string2[j-1] + alignment_b
            j -= 1

    print(animal_name_1 + "--" + animal_name_2 + ": " + str(solution_matrix[len(string1)][len(string2)]))
    print(alignment_a)
    print(alignment_b)




animals = load_animals("fasta.txt")
list_of_indices = list(range(len(animals)))
perms = list(itertools.combinations(list_of_indices, r=2))

for pair in perms:
    word1 = animals[pair[0]][1]
    animal_name_1 = animals[pair[0]][0]
    word2 = animals[pair[1]][1]
    animal_name_2 = animals[pair[1]][0]
    print_solution(animal_name_1, word1, animal_name_2, word2, solve(word1, word2))


