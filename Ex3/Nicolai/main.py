matrixPath = "BLOSUM62.txt"
matrixOfScoresPreliminary = []
matrixFile = open(matrixPath, "r")

for (index, line) in zip(range(30), matrixFile):
    if index > 6:
        matrixOfScoresPreliminary.append(line.replace("  ", ",").replace(" ", ",").strip().split(",")[1:-1])

matrixOfScores = [[int(y) for y in x] for x in matrixOfScoresPreliminary]
gapScore = matrixOfScores[-1][-1]


def letters_to_score(a, b):
    return matrixOfScores[ord(a.lower())-97][ord(b.lower())-97]


def alignment(X, Y):

    A = [[0]*(len(Y)+1) for n in range(len(X)+1)]

    best = 0
    optloc = (0,0)

    for j in range(1, len(X)+1):
        for i in range(1, len(Y)+1):
            A[i][j] = max(
                A[i][j-1] + gapScore,
                A[i-1][j] + gapScore,
                A[i-1][j-1] + letters_to_score(X[i-1], Y[i-1])
            )

            if A[i][j] >= best:
                best = A[i][j]
                optloc = (i, j)

    return A


print(alignment("Super", "mands"))
