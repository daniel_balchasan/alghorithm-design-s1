public class Girl {
    int id;
    Guy[] pref;
    Guy currentMatching;
    int currentMatchingIndex;

    public Girl(Guy[] pref) {

        this.pref = pref;
        this.currentMatching = null;
        this.currentMatchingIndex = pref.length;
    }
    public boolean receivePropose(Guy proposer){
        int proposerIndex = checkPref(proposer);
        if(proposerIndex > this.currentMatchingIndex) {
            if(currentMatching != null)
                currentMatching.breakEngagement();
            currentMatching = proposer;
            currentMatchingIndex = proposerIndex;
            return true;
        }
        else
            return false;

    }
    private int checkPref (Guy proposer){
        for (int i = 0; i< pref.length;i++){
            if(pref[i] == proposer)
                return i;
        }
        return -1;

    }

}
