public class Guy {
    int ID;
    Girl[] pref;
    int proposalIndex = -1;
    boolean isMatched = false;

    public Guy(int ID) {
        this.ID = ID;
    }

    public void proposeNext(){
        if(pref[proposalIndex + 1].receivePropose(this))
            isMatched = true;
        proposalIndex++;
    }
    public void breakEngagement(){
        this.isMatched = false;
    }
    public boolean checkMatched(){
        return this.isMatched;
    }
}
