package com.company;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class FileParser {
    final private String inputDirectory;
    final private String outputDirectory;
    public int n;
    final private Map<Integer, String> nameMap;
    Integer[][] guysPreferences;
    Integer[][] girlsPreferences;

    public FileParser(String inputDirectory, String outputDirectory) {
        this.inputDirectory = inputDirectory;
        this.outputDirectory = outputDirectory;
        this.nameMap = new HashMap();

        File file = new File(this.inputDirectory);
        try {
            Scanner sc = new Scanner(file);
            this.setN(sc);
            this.guysPreferences = new Integer[this.n][this.n];
            this.girlsPreferences = new Integer[this.n][this.n];
            this.loadData(sc);
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void loadData(Scanner sc) {
        String row;
        for (int i = 0; i < n * 2; i++) { //This assumes that all rows in sc until n= ? are already scanned
            row = sc.nextLine();
            this.setNameMap(row);
        }
        sc.nextLine(); // Skip Spacer
        for (int i = 0; i < n * 2; i++) {
            row = sc.nextLine();
            setPreferences(row);
        }
    }

    private void setN(Scanner sc) {
        String row = sc.nextLine();
        while (!row.substring(0, 1).equals("n")) row = sc.nextLine();
        this.n = Integer.parseInt(row.substring(row.indexOf('=') + 1));
    }

    private void setNameMap(String Row) {
        this.nameMap.put(Integer.parseInt(Row.substring(0, Row.indexOf(' '))) - 1, Row.substring(Row.indexOf(' ')+1));
    }

    private void setPreferences(String row) {

        int pID;
        Integer[] pPref = new Integer[this.n];
        int index = 0;
        int normalizedPID = 0;
        int idEnd;
        int prefID;
        boolean isGuy = true;
        boolean isPrefGuy;
        String[] StringIds = row.split(" ");
        for (String StringID : StringIds) {

            if (!StringID.equals(" ")) {
                if ((StringID.indexOf(':')) != -1) {
                    idEnd = StringID.indexOf(':');
                    pID = Integer.parseInt(row.substring(0, idEnd));
                    isGuy = pID % 2 == 1;
                    normalizedPID = this.normalizeID(pID, isGuy);
                } else {
                    prefID = Integer.parseInt(StringID);
                    isPrefGuy = prefID%2 == 1;
                    pPref[index] = this.normalizeID(prefID, isPrefGuy);
                    index++;
                }

            }
        }
        if (isGuy)
            this.guysPreferences[normalizedPID] = pPref;
        else
            this.girlsPreferences[normalizedPID] = pPref;
    }

    private int normalizeID(int pID, boolean isGuy) {
        if (isGuy)
            return pID / 2;
        else
            return pID / 2 -1;
    }

    public Map<Integer, String> getNameMap() {
        return new HashMap<Integer, String>(nameMap);
    }

    public void outputResults(Integer[] results) {
        for (int i = 0; i < this.n; i++) {
            String str = this.nameMap.get((i * 2)) + " -- " + this.nameMap.get(results[i] * 2 + 1) + System.getProperty("line.separator");
            System.out.print(str);
        }


    }


}

