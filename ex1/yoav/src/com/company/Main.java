package com.company;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        FileParser parser = new FileParser("." + File.separator + args[0],".\\SDTOUT.txt");
        Integer[][] guysPreferences = parser.guysPreferences;
        Integer[][] girlsPreferences = parser.girlsPreferences;

        Integer[][] girlsPreferencesOptimized = new Integer[girlsPreferences.length][girlsPreferences.length];
        for (int i = 0; i < girlsPreferences.length; i++) {
            Integer[] currentGirlPrefs = girlsPreferences[i];
            Integer[] tempPrefs = new Integer[currentGirlPrefs.length];
            for (int j = 0; j < currentGirlPrefs.length; j++) {
                tempPrefs[currentGirlPrefs[j]] = j;
            }
            girlsPreferencesOptimized[i] = tempPrefs;
        }


        LinkedList<Integer> freeGuys = new LinkedList<Integer>();
        for (int i = 0; i < guysPreferences.length; i++) {
            freeGuys.add(i);
        }


        Integer[] girlEngagement = new Integer[girlsPreferences.length];
        Arrays.fill(girlEngagement, -1);
        Integer[] guysProposed = new Integer[guysPreferences.length];
        Arrays.fill(guysProposed, -1);

        while (freeGuys.size() != 0) {
            Integer currentProposer = freeGuys.remove(0);
            Integer proposingToOptionNum = guysProposed[currentProposer.intValue()] + 1;
            Integer girlToProposeTo = guysPreferences[currentProposer][proposingToOptionNum];
            guysProposed[currentProposer] = proposingToOptionNum; //Bumps the current proposel follower
            Integer girlToProposeToCurrentEngagementGuy = girlEngagement[girlToProposeTo];
            boolean isGirlCurrentlyEngaged = girlToProposeToCurrentEngagementGuy != -1;
            if (!isGirlCurrentlyEngaged) {
                girlEngagement[girlToProposeTo] = currentProposer;
            } else {
                if (girlsPreferencesOptimized[girlToProposeTo][currentProposer] < girlsPreferencesOptimized[girlToProposeTo][girlToProposeToCurrentEngagementGuy]) {
                    girlEngagement[girlToProposeTo] = currentProposer;
                    freeGuys.add(girlToProposeToCurrentEngagementGuy);
                } else {
                    freeGuys.add(currentProposer);
                }
            }
        }
        Integer[] guysEngagement = new Integer[guysPreferences.length];
        for (Integer i = 0; i < girlEngagement.length ; i++) {
            guysEngagement[girlEngagement[i]] = i;
        }
        parser.outputResults(guysEngagement);

    }
}
