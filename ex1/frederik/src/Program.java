package program;

import objects.Rejecter;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import objects.Proposer;
import objects.Rejecter;


public class Program {

	static ArrayList<Rejecter> rejecters = new ArrayList<Rejecter>();
	static ArrayList<Proposer> proposers = new ArrayList<Proposer>();
	
	static Map<Rejecter, ArrayList<Proposer>> rejecterRanking = new TreeMap<Rejecter, ArrayList<Proposer>>();
	static Map<Proposer, ArrayList<Rejecter>> proposerRanking = new TreeMap<Proposer, ArrayList<Rejecter>>();
	
	public static void main(String[] args) throws IOException {
		
		doMatching();
		
	}
	
	static void readInput(File file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String st;
		while ((st = br.readLine()) != null) {
		  System.out.println(st);
		  // Arrange txt info into proposers and rejecters
		}
	}
	
	static Map<Rejecter, Proposer> doMatching() throws IOException {
		
		readInput(new File("<writefilenamehere>.txt"));
		
		// Initialize empty treemap
		Map<Rejecter, Proposer> matchings = new TreeMap<Rejecter, Proposer>();
		
		
		// Initialize list of proposers who do not yet have any match
		LinkedList<Proposer> freeProposers = new LinkedList<Proposer>();
		freeProposers.addAll(proposers);
		
		// Loop as long as there are free proposers
		while(!freeProposers.isEmpty()) {
			// Get the next proposer by indexing freeproposers at 0 and remove
			Proposer currentProposer = freeProposers.get(0);
			freeProposers.remove(0);
			// Get current proposers ranked list
			ArrayList<Rejecter> currentProposerRanking = proposerRanking.get(currentProposer);
			System.out.println(currentProposerRanking);
			// Loop through each rejecter in proposers ranking list
			for(Rejecter r: currentProposerRanking) {
				
				// If no match, make match
				if(matchings.get(r) == null) {
					matchings.put(r, currentProposer);
				} else {
					// If rejecter already has a match, we will check if this proposer is better suited
					Proposer otherProposer = matchings.get(r);
					// Get ranking list of current rejecter
					ArrayList<Proposer> currentRejecterRanking = rejecterRanking.get(r);
					// If index of current proposer is lower than the previous match, this proposer is better
					if(currentRejecterRanking.indexOf(currentProposer) < currentRejecterRanking.indexOf(otherProposer)) {
						matchings.put(r, currentProposer);
						// Add the original match to the free proposer list
						freeProposers.add(otherProposer);
					}
				}
			}
		}
		System.out.println(matchings.get(rejecters.get(0)));
		System.out.println(matchings.size());
		return matchings;
	}
	
}
