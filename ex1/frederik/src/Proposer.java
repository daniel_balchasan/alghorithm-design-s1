
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class Proposer {

	String id;
	String name;
	
	public Proposer(String id, String name) {
		this.id = id;
		this.name = name;
	}
}
